SET foreign_key_checks = 0;
DROP TABLE `Oddeleni`, `Lek`,`Pojistovna`, `Lekar`, `Pacient`, `Druh_vysetreni`, `Lekar_Druh_vysetreni`, `Lekar_Skupina`, `Vysetreni`, `Hospitalizace`, `Davkovani`, `Skupina`;


CREATE TABLE Oddeleni (
id INT(11) NOT NULL auto_increment,

nazev VARCHAR(20) NOT NULL,
zkratka VARCHAR(3) NOT NULL,

PRIMARY KEY ( id )
)ENGINE=InnoDB default CHARSET=utf8; 


CREATE TABLE Lekar (
id INT(11) NOT NULL auto_increment,
Oddeleni_id INT(11) NOT NULL,

jmeno VARCHAR(20) NOT NULL,
prijmeni VARCHAR(20) NOT NULL,
password VARCHAR(64) NOT NULL,
role VARCHAR(5) NOT NULL,
telefon INT(9),
email VARCHAR(20) NOT NULL,
aktivni INT(1) NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Oddeleni_id ) REFERENCES Oddeleni( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 

CREATE TABLE Pojistovna  (
id INT(11) NOT NULL auto_increment,

kod INT(3) NOT NULL,
nazev VARCHAR(50) NOT NULL,

PRIMARY KEY ( id )
)ENGINE=InnoDB default CHARSET=utf8; 



CREATE TABLE Pacient (
id INT(11) NOT NULL auto_increment,
Pojistovna_id INT(3) NOT NULL,

RC VARCHAR(11) NOT NULL,
jmeno VARCHAR(20) NOT NULL,
prijmeni VARCHAR(20) NOT NULL,
krev_skup VARCHAR(3),
pohlavi VARCHAR(1) NOT NULL,
hmotnost DOUBLE,
telefon INT(9),
tel_blizOs INT(9),

PRIMARY KEY ( id ),
FOREIGN KEY ( Pojistovna_id ) REFERENCES Pojistovna( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 





CREATE TABLE Skupina  (
id INT(11) NOT NULL auto_increment,

nazev VARCHAR(20) NOT NULL,

PRIMARY KEY ( id )
)ENGINE=InnoDB default CHARSET=utf8; 

CREATE TABLE Lek (
id INT(11) NOT NULL auto_increment,
Skupina_id INT(11) NOT NULL,

nazev VARCHAR(20) NOT NULL,
sila DOUBLE NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Skupina_id ) REFERENCES Skupina( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 


CREATE TABLE Druh_vysetreni (
id INT(11) NOT NULL auto_increment,
Oddeleni_id INT(11) NOT NULL,
nazev VARCHAR(25) NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Oddeleni_id ) REFERENCES Oddeleni( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 


CREATE TABLE Lekar_Druh_vysetreni (
id INT(11) NOT NULL auto_increment,
Druh_vysetreni_id INT(11) NOT NULL,
Lekar_id INT(11) NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Druh_vysetreni_id ) REFERENCES Druh_vysetreni( id ) ON DELETE CASCADE,
FOREIGN KEY ( Lekar_id ) REFERENCES Lekar( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 


CREATE TABLE Lekar_Skupina (
id INT(11) NOT NULL auto_increment,
Skupina_id INT(11) NOT NULL,
Lekar_id INT(11) NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Skupina_id ) REFERENCES Skupina( id ) ON DELETE CASCADE,
FOREIGN KEY ( Lekar_id ) REFERENCES Lekar( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 


CREATE TABLE Vysetreni (
id INT(11) NOT NULL auto_increment,
Pacient_id INT(11) NOT NULL,
Lekar_id INT(11) NOT NULL,
Druh_vysetreni_id INT(11) NOT NULL,

datum DATE NOT NULL,
vysledek VARCHAR(500) NOT NULL,
biochemie VARCHAR(50),
hematologie VARCHAR(50),
anamneza VARCHAR(500) NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Pacient_id ) REFERENCES Pacient( id ) ON DELETE CASCADE,
FOREIGN KEY ( Lekar_id ) REFERENCES Lekar( id ) ON DELETE CASCADE,
FOREIGN KEY ( Druh_vysetreni_id ) REFERENCES Druh_vysetreni( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 


CREATE TABLE Hospitalizace (
id INT(11) NOT NULL auto_increment,
Lekar_id INT(11) NOT NULL,
Oddeleni_id INT(11) NOT NULL,
Pacient_id INT(11) NOT NULL,

od DATE NOT NULL,
do DATE,
diagnoza VARCHAR(100) NOT NULL,

PRIMARY KEY ( id ),
FOREIGN KEY ( Lekar_id ) REFERENCES Lekar( id ) ON DELETE CASCADE,
FOREIGN KEY ( Oddeleni_id ) REFERENCES Oddeleni( id ) ON DELETE CASCADE,
FOREIGN KEY ( Pacient_id ) REFERENCES Pacient( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 

CREATE TABLE Davkovani ( 
id INT(11) NOT NULL auto_increment,
Pacient_id INT(11) NOT NULL,
Lekar_id INT(11) NOT NULL,
Lek_id INT(11) NOT NULL,

od DATE NOT NULL,
do DATE,

PRIMARY KEY ( id ),
FOREIGN KEY ( Lek_id ) REFERENCES Lek( id ) ON DELETE CASCADE,
FOREIGN KEY ( Lekar_id ) REFERENCES Lekar( id ) ON DELETE CASCADE,
FOREIGN KEY ( Pacient_id ) REFERENCES Pacient( id ) ON DELETE CASCADE
)ENGINE=InnoDB default CHARSET=utf8; 

INSERT INTO Oddeleni
VALUES (1, 'Chirurgie','CHR');

INSERT INTO Oddeleni
VALUES (2, 'Usni, Nosni, Krcni','ORL');

INSERT INTO Oddeleni
VALUES (3, 'Psychiatrie','PSY');

INSERT INTO Oddeleni
VALUES (4, 'Gynekologie','GYN');

INSERT INTO Oddeleni
VALUES (5, 'Ocni','OCN');

INSERT INTO Lekar
VALUES (1,2,'Hana','Brychtová','$2a$07$9mqp7qyuc1ye1c6jhmenyunrwIYiNixMU5.sjdnnpO/Dt.5EZg.tK','redit',NULL,'brychORL@tux.cz',1);

INSERT INTO Lekar
VALUES (2,1,'Marta','Čudová','$2a$07$9mqp7qyuc1ye1c6jhmenyunrwIYiNixMU5.sjdnnpO/Dt.5EZg.tK','lekar',NULL,'cudovCHR@tux.cz',1);

INSERT INTO Druh_vysetreni
VALUES (1,1,'Predoperacni vysetreni');

INSERT INTO Druh_vysetreni
VALUES (2,1,'Pooperacni vysetreni');

INSERT INTO Druh_vysetreni
VALUES (3,2,'Otoskopie');

INSERT INTO Druh_vysetreni
VALUES (4,2,'Rhinoskopie');

INSERT INTO Druh_vysetreni
VALUES (5,2,'Inspekce dutiny ústní');

INSERT INTO Druh_vysetreni
VALUES (6,2,'Laryngoskopie');

INSERT INTO Druh_vysetreni
VALUES (7,2,'Diafanoskopie');

INSERT INTO Druh_vysetreni
VALUES (8,3,'Rorchachuv test');

INSERT INTO Druh_vysetreni
VALUES (9,3,'Terapie');

INSERT INTO Druh_vysetreni
VALUES (10,4,'Preventivni prohlidka');

INSERT INTO Druh_vysetreni
VALUES (11,4,'Tehotensky test');

INSERT INTO Druh_vysetreni
VALUES (12,4,'Predporodni vysetreni');

INSERT INTO Druh_vysetreni
VALUES (13,5,'Zrakovy test');

INSERT INTO Druh_vysetreni
VALUES (14,5,'Preventivni prohlidka');

INSERT INTO Druh_vysetreni
VALUES (15,5,'Test sedeho zakalu');

INSERT INTO Skupina
VALUES (1,'Analgetika');

INSERT INTO Skupina
VALUES (2,'Ocni kapky');

INSERT INTO Skupina
VALUES (3,'Antidepresiva');

INSERT INTO Skupina
VALUES (4,'Antibiotika');

INSERT INTO Lek
VALUES (1,1,'Paralen',200);

INSERT INTO Lek
VALUES (2,1,'Paralen',500);

INSERT INTO Lek
VALUES (3,1,'Ibalgin',300);

INSERT INTO Lek
VALUES (4,2,'Okotok',15);

INSERT INTO Lek
VALUES (5,3,'Novell',100);

INSERT INTO Lek
VALUES (6,3,'Antipsy',200);

INSERT INTO Lek
VALUES (7,4,'Penicilin',200);

INSERT INTO Lek
VALUES (8,4,'Adoril',800);

INSERT INTO Pojistovna
VALUES(1,111,'Všeobecná zdravotní pojišťovna ČR');

INSERT INTO Pojistovna
VALUES(2,201,'Vojenská zdravotní pojišťovna ČR');

INSERT INTO Pojistovna
VALUES(3,205,'Česká průmyslová zdravotní pojišťovna');

INSERT INTO Pojistovna
VALUES(4,207,'Oborová zdravotní poj. zam. bank, poj. a stav.');

INSERT INTO Pojistovna
VALUES(5,209,'Zaměstnanecká pojišťovna Škoda');

INSERT INTO Pojistovna
VALUES(6,211,'Zdravotní pojišťovna ministerstva vnitra ČR');

INSERT INTO Pojistovna
VALUES(7,213,'Revírní bratrská pokladna, zdrav. pojišťovna');


