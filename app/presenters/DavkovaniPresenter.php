<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Application\UI;
use Nette\Forms\Form;

class DavkovaniPresenter extends BasePresenter
{
        /** @var Lekar */
        private $lekar;
        public $typy;


        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                }
                
                
        }


        public function injectPresenter(Model $lekar) {
                $this->lekar = $lekar;
        }

        public function createComponentNoveDavkForm() {

          $form = new UI\Form;
          //$controlPrototype = $form->getElementPrototype()->id('nitab'); 
          $form->addSubmit('add', 'Pridej Davkovani');

                
          // call method signInFormSucceeded() on success
          $form->onSuccess[] = $this->addHosp;
          $form->addProtection();
          return $form;
        }

       public function addHosp()
       {
        $this->redirect('Davkovani:pridej');
       }

       public function createComponentPridejDavkForm() {

          $form = new UI\Form;          
          $controlPrototype = $form->getElementPrototype()->id('nitab'); 
          //najdeme si skupiny leku
          $skupiny = $this->lekar->findSkupinyPodleLekare($this->user->id);
          //findSkupinyPodleLekare($id_lek)
  
          $doSelectu = array();
          foreach ($skupiny as $skupina)
          {

            //echo "Skupina: ".$skupina->nazev;
            $related_leky = array();
            foreach ($skupina->skupina->related("Lek") as $lek) {
              //echo "Typ: ".$lek->nazev;
              //dump($lek->Lekar_id);
              
              $related_leky[$lek->id] = "Nazev: ".$lek->nazev.", sila: ".$lek->sila;
            }
            
            //exit(22);
            $doSelectu[$skupina->skupina->nazev] = $related_leky;
          }
          
 //dump($doSelectu);
//exit(1);
          //od      date    
          $form->addText('od', 'Datum naordinovani:')
                        ->setDefaultValue(date('Y-n-j'))
                        ->setRequired('Je treba zadat dnesni datum');
          //Pacient_id    int(11) 
          $form->addText('RC', 'Rodne cislo pacienta:',11)
                        ->addRule(Form::PATTERN, 'RC musi byt ve tvaru: 6 cislic / 4 cislice', '[0-9]{6}/[0-9]{4}')
                        ->setRequired('Je treba zadat RC Pacienta v prislusnem tvaru.');
          //Lek_id  int(11) 
          //$prompt = Html::el('option')->setText("Zvolte lek")->class('prompt');
          $form->addSelect('Lek_id', 'Lek:', $doSelectu)
                        ->setRequired('Je treba vybrat druh leku.')
                        ->setPrompt("-vyberte-");
           
          //do      date NULL        

          $form->addSubmit('send', 'Odeslat');

                
          // call method signInFormSucceeded() on success
          $form->onSuccess[] = $this->sendData;
          $form->addProtection();
          return $form;
        }

        public function sendData($form)
         {
            $values = $form->getValues();
                try {
                      $values['Lekar_id'] = $this->user->id;
                      
                      //jeste musim ohlidat, jestli pacient existuje...
                      $pacient = $this->lekar->najdiPac($values['RC']);
                      if (!$pacient)
                       {
                         $this->flashMessage('Pacient s timto rodnym cislem neexistuje.');
                       }
                      else
                      {
                        //foreach je takovy maly hack
                        foreach ($pacient as $pac) {
                            $values['Pacient_id'] = $pac->id;
                              //insert do davnkovani
                              unset($values['RC']);
                              $this->lekar->pridejDavk($values);
                              $this->flashMessage('Uspesne se pridalo davkovani do databaze.');
                              $this->redirect('Davkovani:');
                             }
                      }
                      

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
         }

        public function renderDefault($id = 0)
        {
          //ukonceni zvolene hosp a aktualizece
          if ($id!=0)
          {
           $values['do'] = date('Y-n-j');
           //echo $values['do'];
           $this->lekar->updateDavk($values,$id);
           $this->redirect('Davkovani:');
          }
          
          $this->template->davkovani = $this->lekar->najdiDavk($this->user->id);

        }
}