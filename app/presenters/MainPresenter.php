<?php

class MainPresenter extends BasePresenter
{
	/** @var Lekar */
	private $lekar;

        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                }
        }


	public function injectPresenter(Model $lekar) {
		$this->lekar = $lekar;
	}


	 public function renderSeznam()
	 {
          $this->template->reditel = $this->lekar->najdiRed();

          $this->template->oddeleni = $this->lekar->najdiOddeleni();

          $this->template->lekari = $this->lekar->najdiLekare();

	 }
}
