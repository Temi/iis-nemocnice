<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Application\UI;
use Nette\Forms\Form;

class HospitalizacePresenter extends BasePresenter
{
        /** @var Lekar */
        private $lekar;
        public $typy;


        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                }
                
                
        }


        public function injectPresenter(Model $lekar) {
                $this->lekar = $lekar;
        }

        public function createComponentNovaHospForm() {

          $form = new UI\Form;
          //$controlPrototype = $form->getElementPrototype()->id('nitab'); 
          $form->addSubmit('add', 'Pridej Hospitalizaci');

                
          // call method signInFormSucceeded() on success
          $form->onSuccess[] = $this->addHosp;
          $form->addProtection();
          return $form;
        }

       public function addHosp()
       {
        $this->redirect('Hospitalizace:pridej');
       }

       public function createComponentPridejHospForm() {

          $form = new UI\Form;          
          $controlPrototype = $form->getElementPrototype()->id('nitab'); 
 
          
          //od      date  
           $form->addText('od', 'Datum vysetreni:')
                        ->setDefaultValue(date('Y-n-j'))
                        ->setRequired('Je treba zadat dnesni datum');
          $form->addText('RC', 'Rodne cislo pacienta:',11)
                        ->addRule(Form::PATTERN, 'RC musi byt ve tvaru: 6 cislic / 4 cislice', '[0-9]{6}/[0-9]{4}')
                        ->setRequired('Je treba zadat RC Pacienta v prislusnem tvaru.');
          //diagnoza        varchar(100)
          $form->addTextArea('diagnoza', 'Diagnoza:', 40, 5)
                        ->setRequired('Je treba zadat diagnozu.')
                        ->addRule(Form::PATTERN, 'Diagnoza: Maximalne 100 znaku', '[\s\S]{1,100}');


          $form->addSubmit('send', 'Odeslat');

                
          // call method signInFormSucceeded() on success
          $form->onSuccess[] = $this->sendData;
          $form->addProtection();
          return $form;
        }

        public function sendData($form)
         {
            $values = $form->getValues();
                try {
                      $values['Lekar_id'] = $this->user->id;
                      $values['Oddeleni_id'] = $this->lekar->getLekare($this->user->id)->Oddeleni_id;
                      //jeste musim ohlidat, jestli pacient existuje...
                      $pacient = $this->lekar->najdiPac($values['RC']);
                      if (!$pacient)
                       {
                         $this->flashMessage('Pacient s timto rodnym cislem neexistuje.');
                       }
                      else
                      {

                        
                        foreach ($pacient as $pac)
                          {
                            $values['Pacient_id'] = $pac->id;

                            //kontrola, neni-li pacient nahodou uz hospitalizovan (tzn. nemuze byt hospitalizovan 2x)
                            $hospitalizace = $this->lekar->najdiPacVHosp($pac->id);
                            if (!$hospitalizace)
                            {
                              unset($values['RC']);
                              $this->lekar->pridejHosp($values);
                              $this->flashMessage('Uspesne se pridala hospitalizace do databaze.');
                              $this->redirect('Hospitalizace:');
                            }
                            else
                              $this->flashMessage('Pacient je jiz hospitalizovan.');
                            
                          }
                       
                        
                        
                      }
                      

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
         }

        public function renderDefault($id = 0)
        {
          //ukonceni zvolene hosp a aktualizece
          if ($id!=0)
          {
           $values['do'] = date('Y-n-j');
           //echo $values['do'];
           $this->lekar->updateHosp($values,$id);
           $this->redirect('Hospitalizace:');
          }
          
          $this->template->hospitalizace = $this->lekar->najdiHosp($this->user->id);
          
         
           

        }
}