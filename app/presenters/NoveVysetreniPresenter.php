<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Application\UI;
use Nette\Forms\Form;

class NoveVysetreniPresenter extends BasePresenter
{
        /** @var Lekar */
        private $lekar;
        public $typy;


        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                } 
        }


        public function injectPresenter(Model $lekar) {
                $this->lekar = $lekar;
        }

        /**
         * Sign-in form factory.
         * @return Nette\Application\UI\Form
         */
        protected function createComponentSignInForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 

                $typy = $this->lekar->findDruhyVysetreni($this->user->id);
                
                //19.11. 2013 pridano omezeni textovych poli
                $form->addText('datum', 'Datum vysetreni:')
                        ->setDefaultValue(date('Y-n-j'))
                        ->setRequired('Je treba zadat dnesni datum');

                //tohle nechci pridat do databaze....
                $form->addText('RC', 'Rodne cislo pacienta:',11)
                        ->addRule(Form::PATTERN, 'RC musi byt ve tvaru: 6 cislic / 4 cislice', '[0-9]{6}/[0-9]{4}')
                        ->setRequired('Je treba zadat RC Pacienta v prislusnem tvaru.');

                $form->addSelect('Druh_vysetreni_id', 'Typ vysetreni:', $typy)
                        ->setRequired('Je treba vybrat druh vysetreni.')
                        ->setPrompt("-vyberte-");
                $form->addTextArea('anamneza', 'Anamneza:', 40, 5)
                        ->setRequired('Je treba zadat anamnezu.')
                        ->addRule(Form::PATTERN, 'Anamneza: Maximalne 500 znaku', '[\s\S]{1,500}');

                $form->addTextArea('biochemie', 'Biochemie:', 40, 2)
                      ->addCondition(Form::FILLED)
                         ->addRule(Form::PATTERN, 'Biochemie: Maximalne 50 znaku', '[\s\S]{1,50}');

                $form->addTextArea('hematologie', 'Hematologie:', 40, 2)
                      ->addCondition(Form::FILLED)
                          ->addRule(Form::PATTERN, 'Hematologie: Maximalne 50 znaku', '[\s\S]{1,50}');

                $form->addTextArea('vysledek', 'Vysledek vysetreni:', 40, 5)
                        ->setRequired('Je treba zadat vysledek vysetreni.')
                        ->addRule(Form::PATTERN, 'Vysledek: Maximalne 500 znaku', '[\s\S]{1,500}');
              
                    

                $form->addSubmit('send', 'Odeslat');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->sendData;
                $form->addProtection();
                return $form;
        }

          public function sendData($form)
         {
            $values = $form->getValues();
                try {
                      $values['Lekar_id'] = $this->user->id;
                      //jeste musim ohlidat, jestli pacient existuje...
                      $pacient = $this->lekar->najdiPac($values['RC']);
                      if (!$pacient)
                       {
                         $this->flashMessage('Pacient s timto rodnym cislem neexistuje.');
                       }
                      else
                      {
                        foreach ($pacient as $pac)
                          $values['Pacient_id'] = $pac->id; 

                        
                        unset($values['RC']);
                        $this->lekar->pridejVysetreni($values);
                        $this->flashMessage('Uspesne se pridalo vysetreni do databaze.');
                        $this->redirect('NoveVysetreni:');
                      }
                      

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
         }

}