<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Application\UI;
use Nette\Forms\Form;

class PridatPacientaPresenter extends BasePresenter
{
        /** @var Lekar */
        private $lekar;
        public $typy;


        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                }     
        }


        public function injectPresenter(Model $lekar) {
                $this->lekar = $lekar;
        }

        /**
         * Sign-in form factory.
         * @return Nette\Application\UI\Form
         */
        protected function createComponentSignInForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 
                $pojistovny = $this->lekar->findAllPojist();
                
                $form->addText('RC', 'Rodne cislo:',11)
                        ->addRule(Form::PATTERN, 'RC musi byt ve tvaru: 6 cislic / 4 cislice', '[0-9]{6}/[0-9]{4}')
                        ->setRequired('Zadejte pacientovo RC.');
                //RC      varchar(11) 
                $form->addText('jmeno', 'Jmeno:',40,20)
                        ->setRequired('Zadejte jmeno Pacienta.');
                //jmeno   varchar(20)  
                $form->addText('prijmeni', 'Prijmeni:',40,20)
                        ->setRequired('Zadejte prijmeni Pacienta');
                //prijmeni        varchar(20) 
                $form->addText('krev_skup', 'Krevni skupina:',7,3)
                        ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Krevni skupina musi byt ve tvaru A/B/AB/0 +/-', 'A[+-]|B[+-]|0[+-]|AB[+-]');
                //krev_skup       varchar(3) NULL  
                $form->addSelect('pohlavi', 'Pohlaví:', array('m' => 'Muž','f' => 'Žena'))
                        ->setRequired('Zadejte pohlavi pacienta')
                        ->setPrompt("-vyberte-");
                //pohlavi varchar(1)       
                $form->addText('hmotnost', 'Hmotnost v kg:',3)
                        ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Hodnota musi byt cislo','^[1234567890]+(\.?[123456890]+){0,1}$');
                //hmotnost        double NULL     
                $form->addSelect('Pojistovna_id', 'Pojistovna:',$pojistovny)
                        ->setRequired('Zadejte pojistovnu')
                        ->setPrompt("-vyberte-");
                //pojistovna      int(3)
                $form->addText('telefon', 'Telefon:',9,9)
                       ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Hodnota musi byt devitimistne cislo', '[0-9]{9}');
                //telefon int(9)   NULL
                 $form->addText('tel_blizOs', 'Telefon na blizkou osobu:',9,9)
                       ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Hodnota musi byt devitimistne cislo', '[0-9]{9}');
                //tel_blizOs      int(9) NULL


                $form->addSubmit('send', 'Odeslat');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->sendData;
                $form->addProtection();
                return $form;
        }


         public function sendData($form)
         {
            $values = $form->getValues();
            //hack
            if (!$values['telefon'])
                 $values['telefon'] = null;

            if (!$values['tel_blizOs'])
                 $values['tel_blizOs'] = null;

            if (!$values['hmotnost'])
                 $values['hmotnost'] = null;
                try {
                      if ($this->lekar->najdiPac($values['RC'])!= null)
                      {
                       $this->flashMessage('Pacient s takovym rodnym cislem jiz existuje.');
                      }
                      else
                      {
                        $this->lekar->pridejPacient($values);
                        $this->flashMessage('Uspesne se pridal pacient do databaze.');
                        $this->redirect('PridatPacienta:');
                      }

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
         }

}