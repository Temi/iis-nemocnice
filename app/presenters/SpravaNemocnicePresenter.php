<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Application\UI;
use Nette\Forms\Form;

class SpravaNemocnicePresenter extends BasePresenter
{
        /** @var Lekar */
        private $lekar;
        public $typy;


        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                }
                else if (!$this->user->isAllowed('sprava'))
                {
                  $this->flashMessage('Nemate dostatecna prava na zobrazeni teto stranky.');
                  $this->redirect('Main:');
                }     
        }


        public function injectPresenter(Model $lekar) {
                $this->lekar = $lekar;
        }
      


        

        //-----------LEKAR----------------------------------------
        protected function createComponentPridejLekareForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 
                
                $typy = $this->lekar->vypisOddeleni();

                //jmeno   varchar(20)      
                //prijmeni        varchar(20)      
                //password        varchar(11)      
                //role    varchar(5)       
                //telefon int(9) NULL      
                //email   varchar(20)       
                $form->addText('jmeno', 'Jmeno:',40,20)
                        ->setRequired('Zadejte jmeno Lekare.');
                //jmeno   varchar(20)  
                $form->addText('prijmeni', 'Prijmeni:',40,20)
                        ->setRequired('Zadejte prijmeni Lekare');
                //prijmeni        varchar(20) 
                $form->addSelect('Oddeleni_id', 'Oddeleni:', $typy)
                        ->setRequired('Je treba vybrat oddeleni.');

                $form->addText('telefon', 'Telefon:',9,9)
                       ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Hodnota musi byt devitimistne cislo', '[0-9]{9}');
                //telefon int(9)   NULL

                 $form->addPassword('nohashpassword','Heslo:',9,11)
                      ->setRequired('Zadejte heslo Lekare')
                       ->addRule(Form::PATTERN, 'Heslo musi byt dlouhe 3-11 znaku.', '.{3,11}');
  
                $form->addSubmit('send', 'Odeslat');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->pridejLekar;
                $form->addProtection();
                return $form;
        }
  
        public function pridejLekar($form)
         {
            $values = $form->getValues();

            //zahashujeme heslo
            $values['password']=Authenticator::calculateHash($values['nohashpassword']);
            unset($values['nohashpassword']);
            //echo $values['password'];
            //hack
            if (!$values['telefon'])
                 $values['telefon'] = null;

            $a = TRUE;
            $num = 0;
            
            //odstraneni diakritiky i pro multi-byte (napr. UTF-8)
            $prevodni_tabulka = Array(
              'ä'=>'a',
              'Ä'=>'A',
              'á'=>'a',
              'Á'=>'A',
              'à'=>'a',
              'À'=>'A',
              'ã'=>'a',
              'Ã'=>'A',
              'â'=>'a',
              'Â'=>'A',
              'č'=>'c',
              'Č'=>'C',
              'ć'=>'c',
              'Ć'=>'C',
              'ď'=>'d',
              'Ď'=>'D',
              'ě'=>'e',
              'Ě'=>'E',
              'é'=>'e',
              'É'=>'E',
              'ë'=>'e',
              'Ë'=>'E',
              'è'=>'e',
              'È'=>'E',
              'ê'=>'e',
              'Ê'=>'E',
              'í'=>'i',
              'Í'=>'I',
              'ï'=>'i',
              'Ï'=>'I',
              'ì'=>'i',
              'Ì'=>'I',
              'î'=>'i',
              'Î'=>'I',
              'ľ'=>'l',
              'Ľ'=>'L',
              'ĺ'=>'l',
              'Ĺ'=>'L',
              'ń'=>'n',
              'Ń'=>'N',
              'ň'=>'n',
              'Ň'=>'N',
              'ñ'=>'n',
              'Ñ'=>'N',
              'ó'=>'o',
              'Ó'=>'O',
              'ö'=>'o',
              'Ö'=>'O',
              'ô'=>'o',
              'Ô'=>'O',
              'ò'=>'o',
              'Ò'=>'O',
              'õ'=>'o',
              'Õ'=>'O',
              'ő'=>'o',
              'Ő'=>'O',
              'ř'=>'r',
              'Ř'=>'R',
              'ŕ'=>'r',
              'Ŕ'=>'R',
              'š'=>'s',
              'Š'=>'S',
              'ś'=>'s',
              'Ś'=>'S',
              'ť'=>'t',
              'Ť'=>'T',
              'ú'=>'u',
              'Ú'=>'U',
              'ů'=>'u',
              'Ů'=>'U',
              'ü'=>'u',
              'Ü'=>'U',
              'ù'=>'u',
              'Ù'=>'U',
              'ũ'=>'u',
              'Ũ'=>'U',
              'û'=>'u',
              'Û'=>'U',
              'ý'=>'y',
              'Ý'=>'Y',
              'ž'=>'z',
              'Ž'=>'Z',
              'ź'=>'z',
              'Ź'=>'Z'
            );

            
            //parsovani pro email
            $text = $values['prijmeni'];
            $text = strtr($text,$prevodni_tabulka);
            $text = strtolower($text);
            $text = substr($text,0,5);

            $oddBezDia =$this->lekar->oddeleniById($values['Oddeleni_id'])->zkratka;
            $oddBezDia = strtr($oddBezDia,$prevodni_tabulka);
            
            

            while($a == TRUE)
            {
              
              //vytvoreni emailu a kontrola jeho unikatnosti
              if ($num)
                $values['email'] = $text."{$num}".$oddBezDia."@tux.cz";
              else
                $values['email'] = $text.$oddBezDia."@tux.cz";
    
              if (!$this->lekar->findIfEmailExists($values['email']))
                {
                    $a = FALSE;
                    try {
                          $values['aktivni'] = 1;
                          $values['role'] = 'lekar';
                          $this->lekar->pridejLekar($values);
                          
                          $this->flashMessage('Uspesne se pridal lekar do databaze.');
                          $this->redirect('SpravaNemocnice:');
                    } catch (Nette\Security\AuthenticationException $e) {
                          $form->addError($e->getMessage());
                    }
                }
              $num++;
            }
            
         }

         public function createComponentNovyLekarForm() {

          $form = new UI\Form;
          $form->addSubmit('add', 'Pridej Lekare');

                
          // call method signInFormSucceeded() on success
          $form->onSuccess[] = $this->addLekar;
          $form->addProtection();
          return $form;
        }

        public function addLekar()
       {
        $this->redirect('SpravaNemocnice:pridejLekare');
       }

         public function createComponentZmenitRediteleForm() {

          $form = new UI\Form;
          $form->addSubmit('add', 'Zmenit Reditele');

                
          // call method signInFormSucceeded() on success
          $form->onSuccess[] = $this->zmenRedit;
          $form->addProtection();
          return $form;
        }

       public function zmenRedit()
       {
        $this->redirect('SpravaNemocnice:zmenitReditele');
       }

       public function renderZmenitReditele ($id = 0)
       {
        $this->template->lekari = $this->lekar->najdiLekare();
        
        if ($id!=0 && $this->lekar->findById($id)==null)
          {
            $this->flashMessage('Ucet neexistuje.');
            $this->redirect('SpravaNemocnice:');
          }
        else if ($id!=0 && $this->lekar->findById($id)->id==$this->user->getIdentity()->id)
          {
            $this->flashMessage('Jiz reditelem jste.');
            $this->redirect('SpravaNemocnice:');
          }
        else if ($id!=0)
          {
            //zmenit reditele
            $data['role'] = "redit";
            $this->lekar->upravLekar($data,$id);
            //zmenit roli sobe
            $data['role'] = "lekar";
            $this->lekar->upravLekar($data,$this->user->getIdentity()->id);
            //odhlaseni
            $this->redirect("Sign:Out");
          }
        


      
       }




        public function renderUpravLekare($id = 0)
        {
                $form = $this['updateLekarForm'];
                if (!$form->isSubmitted()) {
                        $lekar = $this->lekar->findById($id);
                        if (!$lekar) {
                                $this->flashMessage('Lekar nenalezen');
                                $this->redirect('SpravaNemocnice:');
                        }
                        $form->setDefaults($lekar);
                }
        }

        public function createComponentUpdateLekarForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 
                 $form->addText('jmeno', 'Jmeno:',40,20)
                        ->setRequired('Zadejte jmeno Lekare.');
                //jmeno   varchar(20)  
                $form->addText('prijmeni', 'Prijmeni:',40,20)
                        ->setRequired('Zadejte prijmeni Lekare');
                //prijmeni        varchar(20) 

                $form->addText('telefon', 'Telefon:',9,9)
                       ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Hodnota musi byt devitimistne cislo', '[0-9]{9}');
                //telefon int(9)   NULL

                 $form->addPassword('nohashpassword','Heslo:',9,11)
                      ->setRequired('Zadejte heslo Lekare')
                       ->addRule(Form::PATTERN, 'Heslo musi byt dlouhe 3-11 znaku.', '.{3,11}');
  
                $form->addSubmit('send', 'Odeslat');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->upravLekar;
                $form->addProtection();
                return $form;
          
        }
        public function upravLekar($form)
         {
          $values = $form->getValues();
          $id = (int) $this->getParameter('id');


            //zahashujeme heslo
            $values['password']=Authenticator::calculateHash($values['nohashpassword']);
            unset($values['nohashpassword']);
           //hack
            if (!$values['telefon'])
                 $values['telefon'] =null;

                    try {
                          $this->lekar->upravLekar($values,$id);
                          $this->flashMessage('Uspesne upraveny udaje v databazi.');
                          $this->redirect('SpravaNemocnice:');
                      

                      } catch (Nette\Security\AuthenticationException $e) {
                          $form->addError($e->getMessage());
                      }


         }

        public function renderDefault($id = 0)
        {
          $this->template->lekari = $this->lekar->najdiLekare();

          //jeste kontrola jestli ID existuje!
         if ($id!=0 && $this->lekar->findById($id)==null)
          {
            $this->flashMessage('Ucet neexistuje.');
            $this->redirect('SpravaNemocnice:');
          }

         if($id)
         {
           if ($this->lekar->findById($id)->aktivni == 0)
            {
              $values['aktivni'] = 1;
              $this->lekar->upravLekar($values,$id);
              $this->flashMessage('Ucet aktivovan.');
            }
           else
            {
              $values['aktivni'] = 0;
              $this->lekar->upravLekar($values,$id);
              //ukoncim hospitalizace a davkovani tohoto uctu

              //najde hospitalizace daneho lekare
              //public function najdiHosp($id_lekar)
              $hospitalizace = $this->lekar->najdiHosp($id);
              if($hospitalizace)
              {
               foreach ($hospitalizace as $hosp)
               {
                //public function updateHosp($data,$id)
                $data['do']= date('Y-n-j');
                updateHosp($data,$hosp->id);
               }
              }

              //najde davkovani daneho lekare
              //public function najdiDavk($id_lekar)
              $davkovani = $this->lekar->najdiDavk($id);
              if($davkovani)
              {
               foreach ($davkovani as $davk)
               {
                //public function updateDavk($data,$id)
                $data['do']= date('Y-n-j');
                updateHosp($data,$davk->id);
               }
              }

              $this->flashMessage('Ucet deaktivovan a ukonceny jeho hospitalizace a davkovani.');
            }
            
           
           $this->redirect('SpravaNemocnice:');
         }

        

        }


      //-----------------------------------------------------
      //------DRUHY VYSETRENI------------------------------->.<///
  
        //uceni lekare novym vysetrenim
        public function renderPrirad($id = 0)
        {
          $lekar = $this->lekar->findById($id);
          if (!$lekar) {
                        $this->flashMessage('Lekar nenalezen.');
                        $this->redirect('SpravaNemocnice:');
                       }
          $this->template->lekar = $lekar;
          $this->template->druhyVysetreni = $this->lekar->findDruhyVysetreniLek($id);
          $this->template->skupiny = $this->lekar->najdiSkupinyLek($id);
          //dump($this->template->druhyVysetreni);
          //exit(23);
                      
        }

      protected function createComponentPriradDruh_vysetreniForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 
                

                $typy = $this->lekar->findDruhyVysetreniOdd((int) $this->getParameter('id'));

                //id      int(11) Auto Increment   
                //Oddeleni_id     int(11)  
                //nazev

                $form->addSelect('Druh_vysetreni_id', 'Druh vysetreni:', $typy)
                        ->setRequired('Je treba vybrat druh vysetreni.')
                        ->setPrompt('-vyberte-');

                $form->addSubmit('send', 'Odeslat');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->priradDruh_vysetreni;
                $form->addProtection();
                return $form;
        }

         protected function createComponentPriradSkupinyForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 
                

                $typy = $this->lekar->najdiSkupinyDvojce();

                //id      int(11) Auto Increment   
                //Oddeleni_id     int(11)  
                //nazev

                $form->addSelect('Skupina_id', 'Skupina leku:', $typy)
                        ->setRequired('Je treba vybrat skupinu leku.')
                        ->setPrompt('-vyberte-');

                $form->addSubmit('send', 'Odeslat');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->priradSkupinu;
                $form->addProtection();
                return $form;
        }
        

        public function priradSkupinu($form)
         {
            
            $values = $form->getValues();
            $values['Lekar_id'] = (int) $this->getParameter('id');

            //zadne duplicity aneb co uz mam, to neprirazuju
            if ($this->lekar->findIfLekarCanSku($values['Lekar_id'],$values['Skupina_id']) == false)
              {
                try {
                        $this->lekar->pridejLekar_Skupina($values);
                        $this->flashMessage('Uspesne byla prirazena skupina leku lekari.');
                        $this->redirect('SpravaNemocnice:prirad',$values['Lekar_id']);

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
              }
            else
              $this->flashMessage('Tuto skupinu leku ma jiz lekar prirazenou.');
         }


        public function priradDruh_vysetreni($form)
         {
            
            $values = $form->getValues();
            $values['Lekar_id'] = (int) $this->getParameter('id');

            //zadne duplicity aneb co uz mam, to neprirazuju
            if ($this->lekar->findIfLekarCan($values['Lekar_id'],$values['Druh_vysetreni_id']) == false)
              {
                try {
                        $this->lekar->pridejLekar_Druh_vysetreni($values);
                        $this->flashMessage('Uspesne byl prirazen druh vysetreni lekari.');
                        $this->redirect('SpravaNemocnice:prirad',$values['Lekar_id']);

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
              }
            else
              $this->flashMessage('Toto vysetreni ma jiz lekar prirazeno.');
         }
  



}