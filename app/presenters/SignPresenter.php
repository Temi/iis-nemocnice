<?php

use Nette\Application\UI;
use Nette\Utils\Html;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{


	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
          //dump(Authenticator::calculateHash("xxx"));
          //exit(2);
		$form = new UI\Form;
                

                $controlPrototype = $form->getElementPrototype()->id('logtab'); 

		$form->addText('email', 'Váš Email:')
			->setRequired('Prosím vyplňte svůj email.');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Prosím vyplňte své heslo.');

		$form->addSubmit('send', 'Přihlásit');

		// call method signInFormSucceeded() on success
		$form->onSuccess[] = $this->signInFormSucceeded;
		return $form;
	}

	public function signInFormSucceeded($form)
	{
		$values = $form->getValues();
		try {
			$this->user->login($values->email, $values->password);

                        // přihlášení vyprší po 20 minutách neaktivity nebo zavření prohlížeče
                        $this->user->setExpiration('+ 20 minutes', TRUE);
			$this->redirect('Main:');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}


	public function actionOut()
	{
		$this->user->logout();
                $this->flashMessage('Byl jste odhlášen.');
		$this->redirect('default');
                
	}

}
