<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Application\UI;
use Nette\Forms\Form;

class VyhledatPacientaPresenter extends BasePresenter
{
        /** @var Lekar */
        private $lekar;
        public $results;


        protected function startup()
        {
                parent::startup();

                if (!$this->user->isLoggedIn()) {
                        $this->flashMessage('Pro pristup na tuto stranku musite byt prihlasen.');
                        $this->redirect('Sign:');
                }               
        }


        public function injectPresenter(Model $lekar) {
                $this->lekar = $lekar;
        }

        /**
         * Sign-in form factory.
         * @return Nette\Application\UI\Form
         */
        protected function createComponentSignInForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 
                $pojistovny = $this->lekar->findAllPojist();
                 
                
                $form->addSelect('Pojistovna_id', 'Pojistovna:',$pojistovny)
                      ->setPrompt("-vyberte-");
                $form->addText('prijmeni', 'Prijmeni:',40,20);
                $form->addText('RC', 'Rodne cislo:',11)
                     ->addCondition(Form::FILLED)
                      ->addRule(Form::PATTERN, 'RC musi byt ve tvaru: 6 cislic / 4 cislice', '[0-9]{6}/[0-9]{4}');

                $form->addSubmit('sendall', 'Vyhledat vsechny pacienty')
                      ->onClick[] = callback($this, 'allPacient');

                $form->addProtection();
                return $form;
        }

       public function allPacient(\Nette\Forms\Controls\SubmitButton $button)
         {
            $this->results = array();
			$values = $button->form->getValues();
            if ($values['RC']== null && $values['prijmeni']== null && $values['Pojistovna_id']== null)
            {
              $this->flashMessage('Nezadal jste kriteria, podle kterych vyhledavat.');
            }
            else
            {
              try {
                      if ($values['Pojistovna_id'])
                       {        
                          
                          if ($values['prijmeni'])
                          {
                            if ($values['RC'])          
                            {
                                $this->results = $this->lekar->najdiPac($values['RC'],$values['Pojistovna_id'],$values['prijmeni']);
                            }
							//tady vyhledat jen hosp
                            $radky = $this->lekar->najdiPac(null,$values['Pojistovna_id'],$values['prijmeni']);

							$lekarovyHosp = $this->lekar->najdiHosp($this->user->getId());
							
							foreach ($radky as $radek)
								foreach ($lekarovyHosp as $hosp)
							{
								if ($radek->id == $hosp->Pacient_id)
									array_push($this->results,$radek);
							}

                          }
                           else if ($values['RC'])
                          {
                            $this->results = $this->lekar->najdiPac($values['RC'],$values['Pojistovna_id']);
                          }
                          else
                          {
							//tady vyhledat jen hosp
                            $radky = $this->lekar->najdiPac(null,$values['Pojistovna_id']);

							$lekarovyHosp = $this->lekar->najdiHosp($this->user->getId());
							
							foreach ($radky as $radek)
								foreach ($lekarovyHosp as $hosp)
							{
								if ($radek->id == $hosp->Pacient_id)
									array_push($this->results,$radek);
							}

                          }
                       }
                      else if ($values['prijmeni'])
                       {
                          
                          if ($values['RC'])
                          {
                            $this->results = $this->lekar->najdiPac($values['RC'],null,$values['prijmeni']);
                          }
                          else
                          {
							//tady je hosp
                            $radky = $this->lekar->najdiPac(null,null,$values['prijmeni']);

							$lekarovyHosp = $this->lekar->najdiHosp($this->user->getId());
							
							foreach ($radky as $radek)
								foreach ($lekarovyHosp as $hosp)
							{
								if ($radek->id == $hosp->Pacient_id)
									array_push($this->results,$radek);
							}
                          }
                       }
                        else if ($values['RC'])
                       {
                         $this->results = $this->lekar->najdiPac($values['RC']);
                       }
                      $this->setView('results');

                  } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());

                  }
                }
         }


        public function renderResults ()
        {
          if (!$this->results)
            {
              $this->setView('default'); 
            }
          else
            {
              $this->template->pacienti = $this->results;
            }
        }

        protected function createComponentUpdatePacForm()
        {
                $form = new UI\Form;
                $controlPrototype = $form->getElementPrototype()->id('nitab'); 

                $pojistovny = $this->lekar->findAllPojist();
                
                $form->addText('jmeno', 'Jmeno:',40,20)
                        ->setRequired('Zadejte jmeno Pacienta.');
                //jmeno   varchar(20)  
                $form->addText('prijmeni', 'Prijmeni:',40,20)
                        ->setRequired('Zadejte prijmeni Pacienta');
                //prijmeni        varchar(20) 
                $form->addText('krev_skup', 'Krevni skupina:',7,3)
                        ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Krevni skupina musi byt ve tvaru A/B/AB/0 +/-', 'A[+-]|B[+-]|0[+-]|AB[+-]');
                //krev_skup       varchar(3) NULL  
                $form->addSelect('pohlavi', 'Pohlaví:', array('m' => 'Muž','f' => 'Žena'))
                        ->setRequired('Zadejte pohlavi pacienta')
                        ->setPrompt("-vyberte-");
                //pohlavi varchar(1)       
                $form->addText('hmotnost', 'Hmotnost v kg:',3)
                        ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Hodnota musi byt cislo','^[1234567890]+(\.?[123456890]+){0,1}$');
                        //->addRule(Form::PATTERN, 'Krevni skupina musi byt ve tvaru A/B/AB/0 +/-', 'A[+-]|B[+-]|0[+-]|AB[+-]');
                        //->addRule(Form::PATTERN, 'Hmotnost nesmi byt zaporne cislo!', '^[1-9].*');

                //hmotnost        double NULL     
                $form->addSelect('Pojistovna_id', 'Pojistovna:',$pojistovny)
                        ->setRequired('Zadejte pojistovnu')
                        ->setPrompt("-vyberte-");
                //pojistovna      int(3)
                $form->addText('telefon', 'Telefon:',9,9)
                       ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Telefon musi byt devitimistne cislo', '[1234567890]{9}');
                //telefon int(9)   NULL
                 $form->addText('tel_blizOs', 'Telefon na blizkou osobu:',9,9)
                       ->addCondition(Form::FILLED)
                        ->addRule(Form::PATTERN, 'Telefon na blizkou osobu musi byt devitimistne cislo', '[1234567890]{9}');
                //tel_blizOs      int(9) NULL


                $form->addSubmit('send', 'Aktualizovat udaje');

                
                // call method signInFormSucceeded() on success
                $form->onSuccess[] = $this->updatePac;
                return $form;
        }

         public function updatePac($form)
         {
          $values = $form->getValues();


          //hack
            if (!$values['telefon'])
                 $values['telefon'] = null;

            if (!$values['tel_blizOs'])
                 $values['tel_blizOs'] = null;

            if (!$values['hmotnost'])
                 $values['hmotnost'] = null;

     
          $id = (int) $this->getParameter('id');
              try {
                      $this->lekar->updatePacient($values,$id);
                      $this->flashMessage('Uspesne upraveny udaje v databazi.');
                      $this->redirect('VyhledatPacienta:');
                      

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
         }

        public function renderUprav($id = 0)
        {
                $form = $this['updatePacForm'];
                if (!$form->isSubmitted()) {
                        $pacient = $this->lekar->pacById($id);
                        if (!$pacient) {
                                $this->flashMessage('Pacient nenalezen');
                                $this->redirect('VyhledatPacienta:');
                        }
                        $form->setDefaults($pacient);
                }
        }

       public function renderLecba($id = 0)
        {
         if ($id)
         {
            $pacient = $this->lekar->pacById($id);
            if (!$pacient) 
              {
                $this->flashMessage('Neexistujici pacient.');
                $this->redirect('VyhledatPacienta:');
              }
            else
            {
              $this->template->vysetreni = $this->lekar->pacVys($id);
              $this->template->hospitalizace = $this->lekar->pacHosp($id);
              $this->template->davkovani = $this->lekar->pacDavk($id);
              $this->template->pacient = $pacient;
            }
         }
         else
         {
          $this->flashMessage('Neexistujici pacient.');
          $this->redirect('VyhledatPacienta:');
         }
        }
  
}