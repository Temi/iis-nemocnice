<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/

class Model extends Nette\Object {
    private $db;

    public function __construct($database) {
	$this->db = $database;
    }

    /** @return Nette\Database\Table\ActiveRow */
    public function findById($id)
      {
        return $this->getLekar()->get($id);
      }

    

    // vyber druhu vysetreni podle id lekare
    public function findDruhyVysetreni($id_lek)
    {
      //najde idcka vsech vysetreni, ktere lekar umi
      $data = $this->db->table('Lekar_Druh_vysetreni')->where('Lekar_id',$id_lek)->select('Druh_vysetreni_id');
      
      //najde nazvy tech vybranych vysetreni
      return $this->db->table('Druh_vysetreni')->where('id',$data)->order('nazev')->fetchPairs('id','nazev');
    } 

    //vyber druhy vysetreni podle oddeleni lekare
    public function findDruhyVysetreniOdd($id_lek)
    {

      $data = $this->db->table('Druh_vysetreni')->where('Oddeleni_id',$this->db->table('Lekar')->where('id',$id_lek)->select('Oddeleni_id'))->fetchPairs('id','nazev');

      return $data;
    } 

    //vyber pojistovny
    public function findAllPojist()
    {

     return $this->db->table('Pojistovna')->fetchPairs('id','nazev');

    } 

    // prohledavani tabulky Lekar_Druh_vysetreni za ucelem hledani duplicit
    //funkce vracejici, jestli lekar umi dane vysetreni
    public function findIfLekarCan($id_lek,$id_druhVys)
    {
      if (!$this->db->table('Lekar_Druh_vysetreni')->where('Lekar_id',$id_lek)->where('Druh_vysetreni_id',$id_druhVys)->fetchPairs('id'))
        return false;
      else
        return true;
    }

    //funkce vracejici lekatovy druhy vysetreni
    public function findDruhyVysetreniLek($id_lek)
    {
      return $this->db->table('Lekar_Druh_vysetreni')->where('Lekar_id',$id_lek)->fetchPairs('id');
 
    }
    

    /**
     * Vrací objekt reprezentující databázovou tabulku.
     * @return Nette\Database\Table\Selection
     */
    protected function getLekar()
    {
      return $this->db->table('Lekar');
    }

    //vraci objekt tabulky pacient
    public function getPacient($pac = 'Pacient')
    {
      return $this->db->table($pac);
    }

    //provede insert do tabulky pacient
    public function pridejPacient($data)
    {
      $this->getPacient()->insert($data);
    }

    //provede update pro dany radek tabulky pacient
    public function updatePacient($data, $id)
    {
      $this->getPacient()->where('id',$id)->update($data);
    }

    //vybere objekt tabulky vysetreni
    public function getVysetreni()
    {
      return $this->db->table('Vysetreni');
    }

    //provede insert do tabulky vysetreni
    public function pridejVysetreni($data)
    {
      $this->getVysetreni()->insert($data);
    }

    //provede insert do tabulky hospitalizace
    public function pridejHosp($data)
    {
      $this->db->table('Hospitalizace')->insert($data);
    }
    
    //provede insert do tabulky davkovani
    public function pridejDavk($data)
    {
      $this->db->table('Davkovani')->insert($data);
    }

    //provede update pro dany radek tabulky hospitalizace
    public function updateHosp($data,$id)
    {
      $this->db->table('Hospitalizace')->where('id',$id)->update($data);
    }

    //provede update pro dany radek tabulky davkovani
    public function updateDavk($data,$id)
    {
      $this->db->table('Davkovani')->where('id',$id)->update($data);
    }

    //vrati dany radek tabulky lekar
    public function getLekare($id)
    {
      return $this->getLekar()->where('id',$id)->fetch();
    }

    //najde hospitalizace daneho lekare
    public function najdiHosp($id_lekar)
    {
      return $this->db->table('Hospitalizace')->where('Lekar_id',$id_lekar)->where('do',null)->fetchPairs('id');      
    }
    
    //overi, je-li dany pacient prave hospitalizovan
    public function najdiPacVHosp($id_pac)
    {
      return $this->db->table('Hospitalizace')->where('Pacient_id',$id_pac)->where('do',null)->fetchPairs('id');
    }

    //najde davkovani daneho lekare
    public function najdiDavk($id_lekar)
    {
      return $this->db->table('Davkovani')->where('Lekar_id',$id_lekar)->where('do',null)->fetchPairs('id');      
    }

    //najde skupiny leku, ktere jsou v databazi
    public function najdiSkupiny()
    {
      //Lekar_Skupina
      return $this->db->table('Skupina')->order('nazev');
    }

     // vyber druhu vysetreni podle id lekare
    public function findSkupinyPodleLekare($id_lek)
    {
      //najde idcka vsech skupin, ktere lekar umi
      return $this->db->table('Lekar_Skupina')->where('Lekar_id',$id_lek)->select('Skupina_id')->fetchPairs('id');
      
      //najde nazvy tech vybranych vysetreni
      //$this->db->table('Lek')->where('Skupina_id',$data)->order('nazev')->fetchPairs('id','nazev');
    } 

    //najde skupiny leku, ktere jsou v databazi pro daneho lekare
    public function najdiSkupinyLek($id_lek)
    {
      //Lekar_Skupina
      return $this->db->table('Lekar_Skupina')->where('Lekar_id',$id_lek)->fetchPairs('id');
    }

    //najde skupiny leku, ktere jsou v databazi a vrati je ve dvojci id - nazev pro pole do formulare
    public function najdiSkupinyDvojce()
    {
      //Lekar_Skupina
      return $this->db->table('Skupina')->order('nazev')->fetchPairs('id','nazev');
    }

    // prohledavani tabulky Lekar_Druh_vysetreni za ucelem hledani duplicit
    //funkce vracejici, jestli lekar umi dane vysetreni
    public function findIfLekarCanSku($id_lek,$id_sk)
    {
      if (!$this->db->table('Lekar_Skupina')->where('Lekar_id',$id_lek)->where('Skupina_id',$id_sk)->fetchPairs('id'))
        return false;
      else
        return true;
    }

    //vyhleda pacienta podle ruznych kriterii, vrati serazene pole podle prijmeni
    public function najdiPac($RC = null,$pojistovna = null,$prijmeni=null)
    {
      if ($RC)
      {
        if ($pojistovna)
          {
            if ($prijmeni)
              {
                return $this->getPacient('Pacient')->where('RC', $RC)->where('Pojistovna_id', $pojistovna)->where('prijmeni', $prijmeni)->order('prijmeni')->fetchPairs('id');
              }
            return $this->getPacient('Pacient')->where('RC', $RC)->where('Pojistovna_id', $pojistovna)->order('prijmeni')->fetchPairs('id');
          }
        else if ($prijmeni)
          {
            return $this->getPacient('Pacient')->where('RC', $RC)->where('prijmeni', $prijmeni)->order('prijmeni')->fetchPairs('id');
          }
        return $this->getPacient('Pacient')->where('RC', $RC)->order('prijmeni')->fetchPairs('id');
      }
      else if ($pojistovna)
      {
        if ($prijmeni)
          {
            return $this->getPacient('Pacient')->where('Pojistovna_id', $pojistovna)->where('prijmeni', $prijmeni)->order('prijmeni')->fetchPairs('id');
          }
        return $this->getPacient('Pacient')->where('Pojistovna_id', $pojistovna)->order('prijmeni')->fetchPairs('id');
      }
      else if ($prijmeni)
      {
        return $this->getPacient('Pacient')->where('prijmeni', $prijmeni)->order('prijmeni')->fetchPairs('id');
      }

      return null;

    }

    
   //vrati pacienta podle jeho id
   public function pacById($id)
   {
      return $this->getPacient('Pacient')->where('id', $id)->fetch();
   } 

   //vrati pacienta podle jeho id
   public function lekById($id)
   {
      return $this->getPacient('Lek')->where('id', $id)->fetch();
   } 

  //najdi vysetreni daneho pacienta
  public function pacVys($id)
   {
      return $this->db->table('Vysetreni')->where('Pacient_id', $id)->fetchPairs('id');
   }

  //najdi hospitalizace daneho pacienta
  public function pacHosp($id)
   {
      return $this->db->table('Hospitalizace')->where('Pacient_id', $id)->fetchPairs('id');
   }

  //najdi davkovani daneho pacienta
  public function pacDavk($id)
   {
      return $this->db->table('Davkovani')->where('Pacient_id', $id)->fetchPairs('id');
   }

  //najdi reditele
  public function najdiRed()
   {
    return $this->db->table('Lekar')->where('role','redit')->fetchPairs('id');
   }

  //najdi vsechna oddeleni
   public function najdiOddeleni()
   {
    $data = $this->db->table('Oddeleni')->order('nazev')->fetchPairs('id');
    return $data;
   }

  //najdi vsechny lekare
   public function najdiLekare()
   {
    return $this->db->table('Lekar')->order('prijmeni')->fetchPairs('id');
   }

  //najdi vsechny leky
   public function najdiLek()
   {
    return $this->db->table('Lek')->order('nazev')->fetchPairs('id');
   }

  //najdi vsechny druhy vysetreni
   public function najdiDruhyVysetreni()
   {
    return $this->db->table('Druh_vysetreni')->order('nazev')->fetchPairs('id');
   }
    
 //--------------SPRAVA NEMOCNICE-----------------------

    //provede insert do tabulky lekar
    public function pridejLekar($data)
    {
      $this->db->table('Lekar')->insert($data);
    }

    //provede insert do tabulky Lekar_Druh_vysetreni
    public function pridejLekar_Druh_Vysetreni($data)
    {
      $this->db->table('Lekar_Druh_vysetreni')->insert($data);
    }

    //provede insert do tabulky Lekar_Druh_vysetreni
    public function pridejLekar_Skupina($data)
    {
      $this->db->table('Lekar_Skupina')->insert($data);
    }

    //provede update tabulky lekar
    public function upravLekar($data,$id)
    {
      $this->db->table('Lekar')->where('id',$id)->update($data);
    }
   
    //najde oddeleni
    public function vypisOddeleni()
    {
      return $this->db->table('Oddeleni')->fetchPairs('id','nazev');
    }

    //najde oddeleni
    public function oddeleniById($id)
    {
      return $this->db->table('Oddeleni')->where('id',$id)->fetch();
    }

    //kontrola unikatnosti emailu
    public function findIfEmailExists($email)
    {
      if (!$this->db->table('Lekar')->where('email',$email)->fetchPairs('id'))
        return false;
      else
        return true;
    }

    

}


