<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Security,
	Nette\Utils\Strings;

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Security\IAuthenticator
{
	/** @var Nette\Database\Connection */
	private $database;


	public function __construct(Nette\Database\Connection $database)
	{
		$this->database = $database;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;
		$row = $this->database->table('Lekar')->where('email', $email)->fetch();

		if (!$row) {
			throw new Security\AuthenticationException('ID je nesprávné.', self::IDENTITY_NOT_FOUND);
		}

		//if ($row->password !== $password) {
                if ($row->password !== self::calculateHash($password, $row->password)) {
			throw new Security\AuthenticationException('Heslo je nesprávné.', self::INVALID_CREDENTIAL);
		}

                // kontrola, neni-li tento ucet deaktivovan
                if ($row->aktivni !== 1) {
                        throw new Security\AuthenticationException('Tento uzivatel neni aktivni.', self::INVALID_CREDENTIAL);
                }

                unset($row->password);
		$arr = $row->toArray();
		return new Nette\Security\Identity($row->id, $row->role, $arr);

               
               //return new NS\Identity($row->id, NULL, $row->toArray());

	}

        public static function calculateHash($password, $salt = null)
        {
          if ($salt === null) {
            $salt = '$2a$07$' . Nette\Utils\Strings::random(22);
                               }
          return crypt($password, $salt);
        }

}
