<?php
/*

        Projekt do predmetu IIS 2013
        ===============================================================
        Název projektu: Nemocnice
        Autorky:                Marta Cudova, xcudov00@stud.fit.vutbr.cz
                                        Hana Brychtova, xbrych02@stud.fit.vutbr.cz
        

*/
use Nette\Security,
        Nette\Utils\Strings;

class Opravneni
{
    /** * @return Nette\Security\Permission */
    public function vytvorOpravneni()
    {
      $acl = new Nette\Security\Permission;

      // definujeme role
      $acl->addRole('lekar');
      $acl->addRole('redit', 'lekar'); 

      $acl->addResource('sprava');

      $acl->allow('redit', 'sprava');
      return $acl;
    }


}